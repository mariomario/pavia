def main():
    print("Whats up students?")

def cluster_corr(corr_array, inplace=False):
    """
    Rearranges the correlation matrix, corr_array, so that groups of highly 
    correlated variables are next to eachother 
    
    Parameters
    ----------
    corr_array : pandas.DataFrame or numpy.ndarray
        a NxN correlation matrix 
        
    Returns
    -------
    pandas.DataFrame or numpy.ndarray
        a NxN correlation matrix with the columns and rows rearranged
    """
    import numpy as np
    import pandas as pd
    import scipy
    import scipy.cluster.hierarchy as sch
    
    corr_array_to_dist = np.sqrt(1.-corr_array)
    pairwise_distances = sch.distance.pdist(corr_array_to_dist)
    linkage = sch.linkage(pairwise_distances, method='ward')
    cluster_distance_threshold = pairwise_distances.max()/5
    idx_to_cluster_array = sch.fcluster(linkage, cluster_distance_threshold, 
                                        criterion='distance')
    idx = np.argsort(idx_to_cluster_array)
    
    if not inplace:
        corr_array = corr_array.copy()
    
    if isinstance(corr_array, pd.DataFrame):
        return corr_array.iloc[idx, :].T.iloc[idx, :]
    return corr_array[idx, :][:, idx]

if __name__ == "__main__":
    main()

